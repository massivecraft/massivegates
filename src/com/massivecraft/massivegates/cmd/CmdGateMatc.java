package com.massivecraft.massivegates.cmd;

import com.massivecraft.massivecore.MassiveException;
import com.massivecraft.massivecore.command.requirement.RequirementHasPerm;
import com.massivecraft.massivecore.command.type.enumeration.TypeMaterial;
import com.massivecraft.massivecore.command.type.primitive.TypeByte;
import com.massivecraft.massivecore.util.Txt;
import com.massivecraft.massivegates.Perm;
import com.massivecraft.massivegates.cmdreq.ReqGateSelected;
import com.massivecraft.massivegates.entity.Gate;
import com.massivecraft.massivegates.entity.MConf;
import org.bukkit.Axis;
import org.bukkit.Material;

import java.util.List;

public class CmdGateMatc extends GateCommand
{
	// -------------------------------------------- //
	// CONSTRUCTOR
	// -------------------------------------------- //
	
	public CmdGateMatc()
	{
		// Parameters
		this.addParameter(TypeMaterial.get(), "material", "get");
		this.addParameter(TypeByte.get(), "data", "null");
		
		// Requirements
		this.addRequirements(ReqGateSelected.get());
		this.addRequirements(RequirementHasPerm.get(Perm.MATC.id));
	}
	
	// -------------------------------------------- //
	// OVERRIDE
	// -------------------------------------------- //
	
	@Override
	public List<String> getAliases()
	{
		return MConf.get().aliasesGateMatc;
	}
	
	@Override
	public void perform() throws MassiveException
	{
		// Internal Args
		Gate gate = gsender.getSelectedGate();
		Material mat = gate.getMatclosed();
		Axis axis = gate.getAxisclosed();
		
		if ( ! this.argIsSet(0))
		{
			this.msg("<i>Current closed <k>Material <i>is <v>%s <i>with <k>Axis <v>%s<i>.", Txt.getMaterialName(mat), axis);
			return;
		}
		
		// Args
		mat = this.readArg();
		axis = this.readArg(null);
		
		if ( ! mat.isBlock())
		{
			this.msg("<h>%s <b>is an item and not a block.", Txt.getMaterialName(mat));
			return;
		}
		
		// Apply
		gate.setMatclosed(mat, axis);
		
		// Inform
		this.msg("<i>New closed <k>Material <i>is <v>%s <i>with <k>Axis <v>%s<i>.", Txt.getMaterialName(mat), axis);
	}
	
}
