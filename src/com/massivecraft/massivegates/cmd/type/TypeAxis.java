package com.massivecraft.massivegates.cmd.type;

import com.massivecraft.massivecore.command.type.enumeration.TypeEnum;
import org.bukkit.Axis;

public class TypeAxis extends TypeEnum<Axis>
{
	// -------------------------------------------- //
	// INSTANCE & CONSTRUCT
	// -------------------------------------------- //
	
	private static TypeAxis i = new TypeAxis();
	public static TypeAxis get() { return i; }
	public TypeAxis()
	{
		super(Axis.class);
	}
	
}
