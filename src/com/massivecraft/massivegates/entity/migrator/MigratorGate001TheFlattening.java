package com.massivecraft.massivegates.entity.migrator;

import com.massivecraft.massivecore.collections.MassiveList;
import com.massivecraft.massivecore.store.migrator.MigratorRoot;
import com.massivecraft.massivecore.xlib.gson.JsonObject;
import com.massivecraft.massivecore.xlib.gson.JsonPrimitive;
import com.massivecraft.massivegates.entity.Gate;
import org.bukkit.Bukkit;
import org.bukkit.Material;

import static org.bukkit.Material.LEGACY_PREFIX;

public class MigratorGate001TheFlattening extends MigratorRoot
{
	// -------------------------------------------- //
	// INSTANCE & CONSTRUCT
	// -------------------------------------------- //
	
	private static MigratorGate001TheFlattening i = new MigratorGate001TheFlattening();
	
	public static MigratorGate001TheFlattening get()
	{
		return i;
	}
	
	public MigratorGate001TheFlattening()
	{
		super(Gate.class);
	}
	
	// -------------------------------------------- //
	// CONVERSION
	// -------------------------------------------- //
	
	@Override
	public void migrateInner(JsonObject entity)
	{
		entity.remove("dataopen");
		entity.remove("dataclosed");
		
		for (String field : new MassiveList<>("matopen", "matclose"))
		{
			JsonPrimitive object = entity.getAsJsonPrimitive(field);
			if (object != null && !object.isJsonNull())
			{
				String legacy = object.getAsString();
				
				if (!legacy.startsWith(LEGACY_PREFIX))
				{
					legacy = LEGACY_PREFIX + legacy;
				}
				
				Material legacyMaterial = Material.getMaterial(legacy);
				if (legacyMaterial == null) return;
				
				// Get New Material
				try
				{
					Material mat = Bukkit.getUnsafe().fromLegacy(legacyMaterial);
					entity.addProperty(field, mat.getKey().getKey().toUpperCase());
				}
				catch (Exception e)
				{
					entity.remove(field);
					System.out.println("Error migrating " + field + " - Set to Default.");
					break;
				}
			}
			
		}
	}
}
